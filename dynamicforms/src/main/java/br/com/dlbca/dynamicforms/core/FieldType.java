package br.com.dlbca.dynamicforms.core;

public enum FieldType {
	
	TEXT, 
	RADIO, 
	COLOR, 
	DATE, 
	DATETIME, 
	DATETIME_LOCAL,
	EMAIL,
	MONTH,
	NUMBER,
	TEL,
	TIME,
	URL,
	WEEK,
	CHECKBOX

}
